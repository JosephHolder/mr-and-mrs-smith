<?php

declare(strict_types=1);

namespace App\Exceptions\Calculator;

use Exception;

class DivideByZeroException extends Exception
{

}

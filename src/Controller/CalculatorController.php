<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exceptions\Calculator\DivideByZeroException;
use App\Exceptions\Calculator\NonNumericException;
use App\Exceptions\Calculator\OperationNotFoundException;
use App\Form\CalculatorType;
use App\Services\Calculator\CalculatorServiceInterface;
use App\Validator\Calculator\CalculatorDataValidator;
use App\Validator\Calculator\CalculatorOperationValidator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validation;

class CalculatorController extends AbstractController
{
    /**
     * @Route("/calculator", name="calculator")
     * @param Request $request
     * @param CalculatorServiceInterface $calculatorService
     * @param CalculatorDataValidator $calculatorDataValidator
     * @param CalculatorOperationValidator $calculatorOperationValidator
     * @return Response
     */
    public function calculate(
        Request $request,
        CalculatorServiceInterface $calculatorService,
        CalculatorDataValidator $calculatorDataValidator,
        CalculatorOperationValidator $calculatorOperationValidator
    ): Response {
        $form = $this->createForm(CalculatorType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            try {
                $calculatorDataValidator->validate($data);
                $calculatorOperationValidator->validate($data['operation']);

                $pageData['result'] = $calculatorService->calculate(
                    (float) $data['firstValue'],
                    (float) $data['secondValue'],
                    $data['operation']
                );
            } catch (NonNumericException | DivideByZeroException | OperationNotFoundException $exception) {
                $form->addError(new FormError($exception->getMessage()));
            }
        }

        $pageData['form'] = $form->createView();
        return $this->render('calculate/form.html.twig', $pageData);
    }
}

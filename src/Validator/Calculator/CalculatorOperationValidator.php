<?php

declare(strict_types=1);

namespace App\Validator\Calculator;

use App\Exceptions\Calculator\OperationNotFoundException;
use App\Services\Calculator\CalculatorService;

class CalculatorOperationValidator
{
    /**
     * @param string $operation
     * @throws OperationNotFoundException
     */
    public function validate(string $operation): void
    {
        if (!in_array($operation, CalculatorService::OPERATIONS_LIST)) {
            throw new OperationNotFoundException('Operation is currently not available');
        }
    }
}

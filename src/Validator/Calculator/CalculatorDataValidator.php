<?php

declare(strict_types=1);

namespace App\Validator\Calculator;

use App\Exceptions\Calculator\NonNumericException;

class CalculatorDataValidator
{
    /**
     * @param array $valueList
     * @throws NonNumericException
     */
    public function validate(array $valueList): void
    {
        foreach ($valueList as $key => $value) {
            if ($key === 'operation') {
                continue;
            }

            if (!is_numeric($value)) {
                throw new NonNumericException(
                    "Fields must contain only numeric values"
                );
            }

            if ((float) $value > PHP_FLOAT_MAX || (float) $value < -PHP_FLOAT_MAX) {
                throw new NonNumericException('Please keep the values inside the php float limits.');
            }
        }
    }
}

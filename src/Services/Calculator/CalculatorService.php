<?php

declare(strict_types=1);

namespace App\Services\Calculator;

use App\Exceptions\Calculator\DivideByZeroException;
use App\Exceptions\Calculator\OperationNotFoundException;

class CalculatorService implements CalculatorServiceInterface
{
    public const ADDITION_OPERATION = '+';
    public const SUBTRACTION_OPERATION = '-';
    public const MULTIPLY_OPERATION = '*';
    public const DIVIDE_OPERATION = '/';

    public const OPERATIONS_LIST = [
        self::ADDITION_OPERATION,
        self::SUBTRACTION_OPERATION,
        self::MULTIPLY_OPERATION,
        self::DIVIDE_OPERATION
    ];

    /**
     * @param float $firstValue
     * @param float $secondValue
     * @return float
     */
    protected function add(float $firstValue, float $secondValue): float
    {
        return (float) ($firstValue + $secondValue);
    }

    /**
     * @param float $firstValue
     * @param float $secondValue
     * @return float
     */
    protected function subtract(float $firstValue, float $secondValue): float
    {
        return (float) ($firstValue - $secondValue);
    }

    /**
     * @param float $firstValue
     * @param float $secondValue
     * @return float
     */
    protected function multiply(float $firstValue, float $secondValue): float
    {
        return (float) ($firstValue * $secondValue);
    }

    /**
     * @param float $firstValue
     * @param float $secondValue
     * @return float
     * @throws DivideByZeroException
     */
    protected function divide(float $firstValue, float $secondValue): float
    {
        if ($secondValue === 0 || empty($secondValue)) {
            throw new DivideByZeroException('The calculator cannot divide by zero. Please review your calculation');
        }

        return (float) ($firstValue / $secondValue);
    }

    /**
     * @param float $firstValue
     * @param float $secondValue
     * @param string $operation
     * @return float
     * @throws DivideByZeroException
     * @throws OperationNotFoundException
     */
    public function calculate(float $firstValue, float $secondValue, string $operation): float
    {
        switch ($operation) {
            case self::ADDITION_OPERATION:
                $value = $this->add($firstValue, $secondValue);
                break;
            case self::SUBTRACTION_OPERATION:
                $value = $this->subtract($firstValue, $secondValue);
                break;
            case self::MULTIPLY_OPERATION:
                $value = $this->multiply($firstValue, $secondValue);
                break;
            case self::DIVIDE_OPERATION:
                $value = $this->divide($firstValue, $secondValue);
                break;
            default:
                throw new OperationNotFoundException('A compatible operation sign could not be found');
        }

        return $value;
    }
}

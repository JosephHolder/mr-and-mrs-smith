<?php

namespace App\Services\Calculator;

interface CalculatorServiceInterface
{
    public function calculate(float $firstValue, float $secondValue, string $operation): float;
}

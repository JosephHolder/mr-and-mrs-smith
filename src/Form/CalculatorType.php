<?php

declare(strict_types=1);

namespace App\Form;

use App\Services\Calculator\CalculatorService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CalculatorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'firstValue',
                TextType::class
            )->add(
                'secondValue',
                TextType::class
            )->add(
                'operation',
                ChoiceType::class,
                [
                    'choices' => array_combine(CalculatorService::OPERATIONS_LIST, CalculatorService::OPERATIONS_LIST)
                ]
            )->add(
                'submit',
                SubmitType::class
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([]);
    }
}

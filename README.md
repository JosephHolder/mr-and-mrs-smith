# Mr and Mrs Smith coding challenge

## Requirements

-  See PHP Developer test.pdf

## Design Choices
- PHP 7.4
- Symfony 5.3

The requirements are to build a calculator with the input data coming from a form. I decided to put the mathematical 
operations in the `CalculatorService.php` as the operations are used only in one place.

## How to run tests
The repository contains both, functional and unit tests to cover as much testing as possible.

First, run `composer install` to install required dependencies.  
Then, `vendor/bin/simple-phpunit`

Code coverage can be seen with `vendor/bin/simple-phpunit --coverage-text`

Both phplint and phpcs were also used, with PSR2 coding style, and can be run as follows:  
`vendor/bin/phplint` and `vendor/bin/phpcs`

## Improvements

### Segregating the mathematical operations to separate services  
The mathematical operations are currently only usable in the calculator service which are in protected methods.
The reason the operations are structured in the `CalculatorService.php` is because they are only used once, so its 
part of the implementation detail of the calculator service.

If we require to use these operations else where in the future. I'd create a separated class and implement them using 
composition instead.

### Precision points
Currently, in the application I have not defined the number of precision points in the application. This is 
not ideal as php can lose data when dealing with numbers greater than 14 decimal places.  

### Number of simultaneous calculations 
Currently, the website can only take in 2 mathematical values. Hopefully in the future we can scale this up 
to sum up more than just 2 values in a given calculation. 

<?php

namespace App\Tests\Services\Calculator;

use App\Exceptions\Calculator\DivideByZeroException;
use App\Exceptions\Calculator\OperationNotFoundException;
use App\Services\Calculator\CalculatorService;
use Generator;
use PHPUnit\Framework\TestCase;

class CalculatorServiceTest extends TestCase
{
    /**
     * @var CalculatorService
     */
    private CalculatorService $calculatorService;

    public function setUp(): void
    {
        $this->calculatorService = new CalculatorService();
    }

    public function testFailInvalidCalculationOperation(): void
    {
        $this->expectException(OperationNotFoundException::class);
        $this->calculatorService->calculate(1, 2, '.');
    }

    public function testFailInvalidDivideByZero(): void
    {
        $this->expectException(DivideByZeroException::class);
        $this->calculatorService->calculate(1, 0, $this->calculatorService::DIVIDE_OPERATION);
    }

    /**
     * Generator
     */
    public function dataFixturesForAddition(): Generator
    {
        yield 'test should pass - both positive integer values returns a float' => [1, 5, 6.0];
        yield 'test should pass - both negative integers values returns a negative float' => [-1, -5, -6.0];
        yield 'test should pass first value is a float' => [1.2, 5, 6.2];
        yield 'test should pass second value is a float' => [1, 5.2, 6.2];
        yield 'test should pass both values are float' => [1.2, 5.2, 6.4];
        yield 'test should pass both values are floats to 12 decimal places' => [
            1.2222222222,
            5.2111111111,
            6.4333333333
        ];
        yield 'test should pass with massive number' => [1.8212472734262E+66, 1.8212472734262E+66, 3.6424945468524E+66];
    }

    /**
     * @dataProvider dataFixturesForAddition
     * @param float $firstValue
     * @param float $secondValue
     * @param float $additionValue
     * @throws DivideByZeroException
     * @throws OperationNotFoundException
     */
    public function testGeneratorForAddition(float $firstValue, float $secondValue, float $additionValue): void
    {
        $additionOperation = $this->calculatorService->calculate(
            $firstValue,
            $secondValue,
            $this->calculatorService::ADDITION_OPERATION
        );

        $this->assertSame($additionOperation, $additionValue);
    }

    /**
     * Generator
     */
    public function dataFixturesForSubtraction(): Generator
    {
        yield 'test should pass - both values integers values returns a float' => [5, 1, 4.0];
        yield 'test should pass first value is a float' => [1.2, 5, -3.8];
        yield 'test should pass second value is a float' => [1, 5.2, -4.2];
        yield 'test should pass both values are float' => [1.2, 5.2, -4.0];
        yield 'test should pass both values are floats to 12 decimal places' => [
            1.2222222222,
            5.2111111111,
            -3.9888888889
        ];
        yield 'test should pass with massive number' => [1.8212472734262E+66, 1.8212472734262E+66, 0.0];
    }

    /**
     * @dataProvider dataFixturesForSubtraction
     * @param float $firstValue
     * @param float $secondValue
     * @param float $result
     * @throws DivideByZeroException
     * @throws OperationNotFoundException
     */
    public function testGeneratorForSubtraction(float $firstValue, float $secondValue, float $result): void
    {
        $additionOperation = $this->calculatorService->calculate(
            $firstValue,
            $secondValue,
            $this->calculatorService::SUBTRACTION_OPERATION
        );

        $this->assertSame($additionOperation, $result);
    }

    /**
     * Generator
     */
    public function dataFixturesForMultiplication(): Generator
    {
        yield 'test should pass - both values integers values returns a float' => [5, 1, 5.0];
        yield 'test should pass first value is a float' => [1.2, 5, 6.0];
        yield 'test should pass second value is a float' => [1, 5.2, 5.2];
        yield 'test should pass both values are float' => [1.2, 5.2, 6.24];
        yield 'test should pass both values are floats to 12 decimal places' => [
            1.2222222222,
            5.2111111111,
            6.369135802339754
        ];
        yield 'test should pass with massive number' => [1.8212472734262E+66, 5, 9.106236367131E+66];
    }

    /**
     * @dataProvider dataFixturesForMultiplication
     * @param float $firstValue
     * @param float $secondValue
     * @param float $result
     * @throws DivideByZeroException
     * @throws OperationNotFoundException
     */
    public function testGeneratorForMultiplication(float $firstValue, float $secondValue, float $result): void
    {
        $additionOperation = $this->calculatorService->calculate(
            $firstValue,
            $secondValue,
            $this->calculatorService::MULTIPLY_OPERATION
        );

        $this->assertSame($additionOperation, $result);
    }

    /**
     * Generator
     */
    public function dataFixturesForDivision(): Generator
    {
        yield 'test should pass - both values integers values returns a float' => [6, 2, 3.0];
        yield 'test should pass first value is a float' => [1.2, 5, 0.24];
        yield 'test should pass second value is a float' => [1, 5.2, 0.1923076923076923];
        yield 'test should pass both values are float' => [1.2, 5.2, 0.23076923076923075];
        yield 'test should pass both values are floats to 12 decimal places' => [
            1.2222222222,
            5.2111111111,
            0.23454157782139562
        ];
        yield 'test should pass with massive number' => [1.8212472734262E+66, 3.6424945468524E+66, 0.5];
    }

    /**
     * @dataProvider dataFixturesForDivision
     * @param float $firstValue
     * @param float $secondValue
     * @param float $result
     * @throws DivideByZeroException
     * @throws OperationNotFoundException
     */
    public function testGeneratorForDivision(float $firstValue, float $secondValue, float $result): void
    {
        $additionOperation = $this->calculatorService->calculate(
            $firstValue,
            $secondValue,
            $this->calculatorService::DIVIDE_OPERATION
        );

        $this->assertSame($additionOperation, $result);
    }
}

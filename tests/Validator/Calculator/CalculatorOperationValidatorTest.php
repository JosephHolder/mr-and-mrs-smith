<?php

declare(strict_types=1);

namespace App\Tests\Validator\Calculator;

use App\Exceptions\Calculator\NonNumericException;
use App\Exceptions\Calculator\OperationNotFoundException;
use App\Services\Calculator\CalculatorService;
use App\Validator\Calculator\CalculatorOperationValidator;
use Generator;
use PHPUnit\Framework\TestCase;

class CalculatorOperationValidatorTest extends TestCase
{
    private CalculatorOperationValidator $calculatorOperationValidator;

    protected function setUp(): void
    {
        $this->calculatorOperationValidator = new CalculatorOperationValidator();
    }

    public function operators(): Generator
    {
        yield 'plusOperator' => ['"'];
        yield 'plusOperator1' => ['.'];
        yield 'plusOperator2' => [','];
        yield 'plusOperator3' => ['%'];
        yield 'plusOperator4' => ['^'];
        yield 'plusOperator5' => ['!'];
    }

    /**
     * @dataProvider operators
     * @throws OperationNotFoundException
     */
    public function testShouldFailWithInCorrectOperations(string $operator): void
    {
        $this->expectException(OperationNotFoundException::class);
        $this->calculatorOperationValidator->validate($operator);
    }
}

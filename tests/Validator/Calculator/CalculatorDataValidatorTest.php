<?php

declare(strict_types=1);

namespace App\Tests\Validator\Calculator;

use App\Exceptions\Calculator\NonNumericException;
use App\Validator\Calculator\CalculatorDataValidator;
use PHPUnit\Framework\TestCase;

class CalculatorDataValidatorTest extends TestCase
{

    private CalculatorDataValidator $calculateDataValidator;

    public function setup(): void
    {
        $this->calculateDataValidator = new CalculatorDataValidator();
    }

    public function testShouldFailWithNonNumericNumber()
    {
        $this->expectException(NonNumericException::class);
        $this->calculateDataValidator->validate(['hello word']);
    }

    public function testShouldFailWithTooLargeNumber()
    {
        $this->expectException(NonNumericException::class);
        $this->calculateDataValidator->validate([1.7976931348624E+308]);
    }
}

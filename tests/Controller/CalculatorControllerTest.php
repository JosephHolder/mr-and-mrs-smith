<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use InvalidArgumentException;

class CalculatorControllerTest extends WebTestCase
{
    public function testShouldFailWithInvalidValue(): void
    {
        $client = static::createClient();
        $client->request("GET", "/calculator");

        $client->submitForm('Submit', [
            'calculator[firstValue]' => 'test',
            'calculator[secondValue]' => '2',
            'calculator[operation]' => '+'
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('li', 'Fields must contain only numeric values');
    }

    public function testShouldFailWithInvalidOperation(): void
    {
        $client = static::createClient();
        $client->request("GET", "/calculator");

        $this->expectException(InvalidArgumentException::class);

        $client->submitForm('Submit', [
            'calculator[firstValue]' => '2',
            'calculator[secondValue]' => '2',
            'calculator[operation]' => '^'
        ]);
    }

    public function testShouldPassPostValueWithOperation(): void
    {
        $client = static::createClient();
        $client->request("GET", "/calculator");

        $client->submitForm('Submit', [
            'calculator[firstValue]' => '2',
            'calculator[secondValue]' => '2',
            'calculator[operation]' => '+'
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('p', '4');
    }
}
